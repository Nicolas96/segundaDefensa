package com.ucbcba.proyecto.proyecto.Entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "zona")
public class Zona {

    private Integer idZona;

    @NotNull
    private String name;

    /*@OneToMany (mappedBy = "zona")
    private Set<Pedido> pedidos;


    public Set<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(Set<Pedido> pedidos) {
        this.pedidos = pedidos;
    }*/

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getIdZona() {
        return idZona;
    }

    public void setIdZona(Integer id) {
        this.idZona = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
